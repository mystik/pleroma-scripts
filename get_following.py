#!/usr/bin/env python3

import requests
import json
from requests.auth import HTTPBasicAuth

user = 'myuser'
password = 'my_password'
instance_domain = 'my-instance.com'
account = 'A19Dtx2sHOHAVIymn2'

def request_following (max_id):
  response = requests.get(
    'https://' + instance_domain + '/api/v1/accounts/' + account + '/following',
    params = {'limit': 40, 'max_id': max_id},
    auth = HTTPBasicAuth(user, password))
  return json.loads(response.text)

json_response = request_following('')
last_id = ''
accounts = []
while True:
  if len(json_response) == 0: break
  for i in json_response:
    accounts.append(i['acct'])
    last_id = i['id']
  print("Retrieved " + str(len(accounts)) + " followed contacts...")
  json_response = request_following(last_id)

print(" ".join(accounts))

